﻿namespace Infrastrucutre.CrossCuting.Tests.Extensions
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Farfetch.Wishlist.Infrastructure.CrossCutting.Extensions;

    [TestClass]
    public class ObjectExtensionTests
    {
        [TestMethod, TestCategory("Infrastructure.CrossCutting")]
        public void IsNull_Test()
        {
            object nullObj = null;
            
            Assert.IsTrue(nullObj.IsNull());
        }

        [TestMethod, TestCategory("Infrastructure.CrossCutting")]
        public void IsNotNull_Test()
        {
            object nullObj = new object();

            Assert.IsTrue(nullObj.IsNotNull());
        }
    }
}
