﻿namespace Farfetch.Wishlist.Domain.Core.Repository
{
    using System;
    using System.Threading.Tasks;
    using Farfetch.Wishlist.Domain.Model;

    public interface IWishListRepository
    {
        Task<WishList> GetWishList(Guid wishListId);
        Task InsertAsync(WishList wishList);
        Task UpdateAsync(WishList wishList);
    }
}
