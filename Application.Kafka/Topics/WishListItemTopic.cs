﻿namespace Application.Kafka.Topics
{
    using Newtonsoft.Json;
    using Application.Kafka.Contracts;
    using Farfetch.Framework.Serializer;
    using Farfetch.Framework.KafkaContracts;
    using Farfetch.Framework.Serializer.Newtonsoft;
    using Farfetch.Framework.KafkaContracts.TopicName;

    public class WishListItemTopic : TopicContract<WishListItemMessage>
    {
        public WishListItemTopic(string environment)
            : base(environment)
        { }

        public override ISerializer Serializer =>
            new NewtonsoftSerializer(
                new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Objects
                });

        public override TopicNameBuilder GetTopicNameBuilder()
        {
            return new TopicNameBuilder //ver melhor isto
            {
                EntityName = "wishListItem",
                MessageType = new EventMessage(),
                Platform = new EcommercePlatform(),
                ServiceName = "account-service",
                Version = "1.0"
            };
        }
    }
}
