﻿namespace Application.Kafka.Producers
{
    using System;
    using System.Threading.Tasks;
    using Farfetch.Framework.Logging;
    using Farfetch.Framework.Kafka.Client;
    using System.Diagnostics.CodeAnalysis;
    using Farfetch.Framework.KafkaContracts;
    using Farfetch.Framework.Kafka.Producer;
    using Farfetch.Framework.Kafka.Configuration;
    using Farfetch.Framework.Kafka.ConfluentKafka.Producer;
    using Farfetch.Wishlist.Infrastructure.CrossCutting.Extensions;

    

    [ExcludeFromCodeCoverage]
    internal abstract class GeneralProducer<MessageContract>
         : IProducer<MessageContract>
        where MessageContract : IMessageContract
    {
        private readonly ILog log;
        private Producer<MessageContract> producer = null;
        protected GeneralProducer(ILog log)
        {
            this.log = log;
        }

        public Producer<MessageContract> Producer
        {
            get
            {
                if (producer.IsNull())
                {
                    this.producer = new Producer<MessageContract>(
                        this.TopicContract,
                        this.ProducerClient,
                        this.ProducerConfiguration);
                }

                return this.producer;
            }
        }

        protected abstract ITopicContract<MessageContract> TopicContract { get; }

        protected abstract ProducerConfiguration ProducerConfiguration { get; }

        protected virtual IProducerClient ProducerClient => new ConfluentProducer();

        public void Dispose()
        {
            this.Producer.Dispose();
        }

        public void Produce(MessageContract messageContract)
        {
            this.Producer.Produce(messageContract);
        }

        public Task<ProducionReport<MessageContract>> ProduceAsync(MessageContract messageContract)
        {
            return this.Producer.ProduceAsync(messageContract);
        }

        public void ProduceAsync(MessageContract messageContract, Action<ProducionReport<MessageContract>> deliveryAction)
        {
            this.Producer.ProduceAsync(messageContract, deliveryAction);
        }
    }
}
