﻿namespace Application.Kafka.Producers
{
    using Application.Kafka.Topics;
    using Farfetch.Framework.Logging;
    using Application.Kafka.Contracts;
    using Application.Kafka.Interfaces;
    using System.Diagnostics.CodeAnalysis;
    using Farfetch.Framework.Kafka.Producer;
    using Farfetch.Framework.KafkaContracts;
    using Farfetch.Framework.Kafka.Configuration;

    [ExcludeFromCodeCoverage]
    internal class WishListItemMessageProducer: GeneralProducer<WishListItemMessage>,IProducer<WishListItemMessage>
    {
        public WishListItemMessageProducer(IKafkaConfiguration settings, ILog logger)
            : base(logger)
        {
            this.ProducerConfiguration =
                new ProducerConfiguration
                {
                    BootstrapServers = settings.BrokerList(),
                    RequestRequiredAcks = ProducerConfiguration.AcksTypes.None,
                    MessageTimeoutMs = int.Parse(settings.Timeout())
                };

            this.TopicContract =
                new WishListItemTopic(settings.Environment());
        }

        protected override ProducerConfiguration ProducerConfiguration { get; }

        protected override ITopicContract<WishListItemMessage> TopicContract { get; }
    }
}
