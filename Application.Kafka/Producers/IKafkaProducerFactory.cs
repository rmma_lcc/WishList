﻿namespace Application.Kafka.Producers
{
    using Application.Kafka.Contracts;
    using Farfetch.Framework.Kafka.Producer;

    public interface IKafkaProducerFactory
    {
        IProducer<T> Create<T>() where T : MessageContract;
    }
}
