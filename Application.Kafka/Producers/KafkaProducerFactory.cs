﻿namespace Application.Kafka.Producers
{
    using System;
    using System.Collections.Generic;
    using Application.Kafka.Contracts;
    using Application.Kafka.Interfaces;
    using System.Diagnostics.CodeAnalysis;
    using Farfetch.Framework.Kafka.Producer;
    using Farfetch.Framework.KafkaContracts;

    [ExcludeFromCodeCoverage]
    public class KafkaProducerFactory: IKafkaProducerFactory
    {
        public IKafkaConfiguration kafkaConfiguration;
        private static Dictionary<Type, object> cachedProducers =
            new Dictionary<Type, object>();

        public KafkaProducerFactory(IKafkaConfiguration kafkaConfiguration)
        {
            this.kafkaConfiguration = kafkaConfiguration;
        }

        public IProducer<T> Create<T>()
            where T : MessageContract
        {
            if (kafkaConfiguration.IsFeatureEnabled())
            {
                if (IsWishListItemMessage<T>())
                {
                    return this.Fetch<T>(() => this.CreateWishListItemMessage());
                }
            }
            return null;
            //return new NullProducer<T>();
        }

        private static bool IsWishListItemMessage<T>() where T : MessageContract
        {
            return IsSameOrSubclass<T, WishListItemMessage>();
        }

        private static bool IsSameOrSubclass<TInput, TExpected>()
            where TInput : MessageContract
            where TExpected : MessageContract
        {
            return typeof(TInput).IsSubclassOf(typeof(TExpected))
                || typeof(TInput) == typeof(TExpected);
        }

        private IProducer<T> Fetch<T>(Func<object> producerCreator)
            where T : MessageContract, IMessageContract
        {
            object output = null;

            if (!cachedProducers.TryGetValue(typeof(T), out output))
            {
                output = producerCreator.Invoke();

                cachedProducers.Add(typeof(T), output);
            }

            return output as IProducer<T>;
        }

        private WishListItemMessageProducer CreateWishListItemMessage()
        {
            return new WishListItemMessageProducer(this.kafkaConfiguration, null);
        }
    }
}
