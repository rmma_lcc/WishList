﻿namespace Application.Kafka
{
    using Application.Kafka.Interfaces;
    using System.Diagnostics.CodeAnalysis;
    using Microsoft.Extensions.Options;

    [ExcludeFromCodeCoverage]
    public class KafkaConfiguration : IKafkaConfiguration
    {
        private readonly bool isKafkaEnabled;
        private readonly string brokerList;
        private readonly string timeout;
        private readonly string environment;

        public KafkaConfiguration(IOptions<ReadKafkaConfiguration> kafkaConfiguration)
        {
            this.isKafkaEnabled = kafkaConfiguration.Value.IsKafkaEnabled;
            this.brokerList = kafkaConfiguration.Value.KafkaBrokerList;
            this.timeout = kafkaConfiguration.Value.KafkaTimeout;
            this.environment = kafkaConfiguration.Value.KafkaEnvironment;
        }
        public bool IsFeatureEnabled() => this.isKafkaEnabled;

        public string BrokerList() => this.brokerList;

        public string Timeout() => this.timeout;

        public string Environment() => this.environment;


    }
}
