﻿


namespace Application.Kafka.Contracts
{
    using System;
    using Farfetch.Wishlist.Domain.Model;
    using Farfetch.Framework.KafkaContracts;

    public abstract class WishListItemMessage : MessageContract, IMessageContract
    {
        protected WishListItemMessage(WishListItem wishListItem)
        {
            this.Id = wishListItem.Id;
            this.ProductId = wishListItem.ProductId;
            this.ProductName = wishListItem.ProductName;
            this.Quantity = wishListItem.Quantity;
            this.CreatedAt = wishListItem.CreatedAt;
            

        }

        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
        public string ProductName { get; set; }


        public override string Uuid =>
            Guid.NewGuid().ToString();

        public override string Version => "1.0";

        public override string GetPartitionKey() =>
            $"{this.Id}";
    }
}
