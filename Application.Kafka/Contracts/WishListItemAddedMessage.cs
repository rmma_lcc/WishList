﻿namespace Application.Kafka.Contracts
{
    using Farfetch.Wishlist.Domain.Model;
    using Farfetch.Framework.KafkaContracts;
    

    public class WishListItemAddedMessage : WishListItemMessage, IMessageContract
    {
        public WishListItemAddedMessage(WishListItem wishListItem): base(wishListItem)
        {}

        
    }
}
