﻿namespace Application.Kafka.Contracts
{
    using Farfetch.Framework.KafkaContracts;

    public abstract class MessageContract : IMessageContract
    {
        public abstract string Uuid { get; }

        public abstract string Version { get; }

        public abstract string GetPartitionKey();
    }
}
