﻿namespace Application.Kafka.Contracts
{
    using System;
    using Farfetch.Framework.KafkaContracts;
    using Farfetch.Wishlist.Domain.Model;

    public class WishListItemUpdatedMessage : WishListItemMessage, IMessageContract
    {
        public WishListItemUpdatedMessage(WishListItem wishListItem,int previousQuantity): base(wishListItem)
        {
            UpdatedAt = DateTime.UtcNow;
            PreviousQuantity = previousQuantity;
        }

        public int PreviousQuantity { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
