﻿namespace Application.Kafka.Contracts
{
    using System;
    using Farfetch.Wishlist.Domain.Model;
    using Farfetch.Framework.KafkaContracts;

    public class WishListItemDeletedMessage : WishListItemMessage, IMessageContract
    {
        public WishListItemDeletedMessage(WishListItem wishListItem) : base(wishListItem)
        {
            this.DeletedAt = DateTime.UtcNow;
        }

        public DateTime DeletedAt {get;set;}
    }
}
