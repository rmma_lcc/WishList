﻿namespace Application.Kafka.Interfaces
{
    public interface IKafkaConfiguration
    {
        bool IsFeatureEnabled();

        string BrokerList();

        string Timeout();

        string Environment();
    }
}
