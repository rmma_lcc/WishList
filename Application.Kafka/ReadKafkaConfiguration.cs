﻿namespace Application.Kafka
{
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class ReadKafkaConfiguration
    {
        public bool IsKafkaEnabled { get; set; }
        public string KafkaBrokerList { get; set; }
        public string KafkaTimeout { get; set; }
        public string KafkaEnvironment { get; set; }
    }
}
