﻿namespace Domain.Model.Tests
{
    using Farfetch.Wishlist.Domain.Model;
    using System;
    using System.Collections.Generic;
    using System.Text;

    public static class WishListBase
    {
        public static WishList CreateWishList()
        {
            var wishList = new WishList(111,112)
            {
                Items = new List<WishListItem>()
                {
                    CreateWishListItem("Product_1","d1ebd7b2-19df-4cd0-93fc-88e5db96fb0f","c1fbd7b2-19df-4cd0-93fc-88e5db96fb0f"),
                    CreateWishListItem("Product_2","d2ebd7b2-19df-4cd0-93fc-88e5db96fb0f","c2fbd7b2-19df-4cd0-93fc-88e5db96fb0f"),
                }
            };

            return wishList;
        }

        public static WishListItem CreateWishListItem(string productName, string guidId, string guidProductId)
        {
            return new WishListItem()
            {
                Id = new Guid(guidId),
                ProductId = new Guid(guidProductId),
                ProductName = productName,
                Quantity = 1,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow
            };
        }
    }
}
