﻿namespace Domain.Model.Tests
{
    using System;
    using Farfetch.Wishlist.Infrastructure.CrossCutting.Extensions;
    using Farfetch.Wishlist.Infrastructure.CrossCutting.Exceptions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class WishListTests
    {
        [TestMethod, TestCategory("Domain.Model")]
        [ExpectedException(typeof(AlreadyExistsException))]
        public void AddItemTest_ProductIdExists_ThrowsAlreadyExistsException()
        {
            var wishList = WishListBase.CreateWishList();

            wishList.AddItem(new Guid("c1fbd7b2-19df-4cd0-93fc-88e5db96fb0f"),"product",1);
        }

        [TestMethod, TestCategory("Domain.Model")]
        [ExpectedException(typeof(InvalidDomainEntityException))]
        public void AddItemTest_ProductNameInvalid_ThrowsInvalidDomainEntityException()
        {
            var wishList = WishListBase.CreateWishList();

            wishList.AddItem(new Guid("a37c6466-c06c-44d8-ac4d-d6f5ef878342"), "", 1);
        }

        [TestMethod, TestCategory("Domain.Model")]
        [ExpectedException(typeof(InvalidDomainEntityException))]
        public void AddItemTest_QuantityInvalid_ThrowsInvalidDomainEntityException()
        {
            var wishList = WishListBase.CreateWishList();

            wishList.AddItem(new Guid("a37c6466-c06c-44d8-ac4d-d6f5ef878342"), "product", 0);
        }

        [TestMethod, TestCategory("Domain.Model")]
        public void AddItemTest()
        {
            var productId = new Guid("a37c6466-c06c-44d8-ac4d-d6f5ef878342");
            var wishList = WishListBase.CreateWishList();

            wishList.AddItem(productId, "product", 1);
            Assert.IsTrue(wishList.Items.Find(item => item.ProductId == productId 
                    && item.ProductName.Equals("product")
                    && item.Quantity==1)
                    .IsNotNull());
        }

        [TestMethod, TestCategory("Domain.Model")]
        [ExpectedException(typeof(ResourceNotFoundException))]
        public void UpdateItemTest_WishListItemNotFound_ThrowsResourceNotFoundException()
        {
            var wishList = WishListBase.CreateWishList();

            wishList.UpdateItem(new Guid("c1fbd7b2-19df-4cd0-93fc-88e5db96fb0f"), 1);
        }

        [TestMethod, TestCategory("Domain.Model")]
        [ExpectedException(typeof(InvalidDomainEntityException))]
        public void UpdateItemTest_QuantityInvalid_ThrowsInvalidDomainEntityExceptionn()
        {
            var wishList = WishListBase.CreateWishList();

            wishList.UpdateItem(new Guid("d1ebd7b2-19df-4cd0-93fc-88e5db96fb0f"), 0);
        }

        [TestMethod, TestCategory("Domain.Model")]
        public void UpdateItemTest()
        {
            var itemId = new Guid("d1ebd7b2-19df-4cd0-93fc-88e5db96fb0f");
            var wishList = WishListBase.CreateWishList();

            wishList.UpdateItem(itemId, 10);
            Assert.IsTrue(wishList.Items.Find(item => item.Id == itemId
                    && item.Quantity == 10)
                    .IsNotNull());
        }

        [TestMethod, TestCategory("Domain.Model")]
        [ExpectedException(typeof(ResourceNotFoundException))]
        public void DeleteItemTest_WishListItemNotFound_ThrowsResourceNotFoundException()
        {
            var wishList = WishListBase.CreateWishList();

            wishList.DeleteItem(new Guid("c1fbd7b2-19df-4cd0-93fc-88e5db96fb0f"));
        }
    }
}
