﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Farfetch.Wishlist.Domain.Model
{
    public abstract class BaseModel
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }

        protected BaseModel()
        {
            this.Id = Guid.NewGuid();
            this.CreatedAt = DateTime.UtcNow;
        }
    }
}
