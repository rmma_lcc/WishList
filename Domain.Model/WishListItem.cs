﻿namespace Farfetch.Wishlist.Domain.Model
{
    using Farfetch.Wishlist.Infrastructure.CrossCutting;
    using Farfetch.Wishlist.Infrastructure.CrossCutting.Exceptions;
    using Farfetch.Wishlist.Infrastructure.CrossCutting.Extensions;
    using System;

    public class WishListItem : BaseModel
    {
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
        public string ProductName { get; set; }
        public DateTime UpdatedAt { get; set; }

        public WishListItem()
        {
        }

        public WishListItem(Guid productId, string productName, int quantity) : base()
        {
            this.ProductId = productId;
            this.ProductName = productName;
            this.Quantity = quantity;
            this.UpdatedAt = DateTime.UtcNow;
        }
    }

    
}
