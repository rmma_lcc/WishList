﻿namespace Farfetch.Wishlist.Domain.Model
{
    using System;
    using System.Collections.Generic;
    using Farfetch.Wishlist.Infrastructure.CrossCutting;
    using Farfetch.Wishlist.Infrastructure.CrossCutting.Exceptions;
    using Farfetch.Wishlist.Infrastructure.CrossCutting.Extensions;
    

    public class WishList : BaseModel
    {
        public int UserId { get; set; }
        public int TenantId { get; set; }
        public List<WishListItem> Items { get; set; }
        

        public WishList() : base()
        {
            this.Items = new List<WishListItem>();
        }

        public WishList(int tenantId, int userId) : this()
        {
            this.UserId = userId;
            this.TenantId = tenantId;
        }

        public WishListItem AddItem(Guid productId, string productName, int quantity)
        {
            var productIdFind = this.Items.Find(item => item.ProductId == productId);
            Guard.Against<AlreadyExistsException>(productIdFind.IsNotNull(), "Product already exists.");
            Guard.Against<InvalidDomainEntityException>(string.IsNullOrWhiteSpace(productName), "Product name cannot be null, empty or whitespace.");
            Guard.Against<InvalidDomainEntityException>(quantity<1, "Quantity is invalid.");

            var wishListItem = new WishListItem(productId, productName, quantity);

            this.Items.Add(wishListItem);

            return wishListItem;
        }

        public Tuple<WishListItem,int> UpdateItem(Guid wishListItemId,int quantity)
        {
            var wishListItemIdFind = this.Items.Find(item => item.Id == wishListItemId);
            Guard.Against<ResourceNotFoundException>(wishListItemIdFind.IsNull(), "WishListItem not found.");
            Guard.Against<InvalidDomainEntityException>(quantity < 1, "Quantity is invalid.");

            var previousQuantidade = wishListItemIdFind.Quantity;

            wishListItemIdFind.Quantity = quantity;
            wishListItemIdFind.UpdatedAt = DateTime.UtcNow;


            return Tuple.Create(wishListItemIdFind, previousQuantidade);
        }

        public WishListItem DeleteItem(Guid wishListItemId)
        {
            var wishListItemIdFind = this.Items.Find(item => item.Id == wishListItemId);
            Guard.Against<ResourceNotFoundException>(wishListItemIdFind.IsNull(), "WishListItem not found.");

            this.Items.Remove(wishListItemIdFind);

            return wishListItemIdFind;
        }
    }
}
