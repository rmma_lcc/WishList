﻿namespace Application.Services.Tests.Extensions
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Farfetch.Wishlist.Domain.Model;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Farfetch.Wishlist.Application.Services.Extensions;

    [TestClass]
    public class MappingExtensionsTests
    {
        private List<WishListItem> wishListItems;

        [TestInitialize]
        public void Initialize()
        {
            this.wishListItems = new List<WishListItem>(){
                    new WishListItem()
                {
                    UpdatedAt = DateTime.Now,
                    ProductId = Guid.NewGuid(),
                    ProductName = "novo_produto",
                    Quantity = 112
                },
                new WishListItem()
                {
                    UpdatedAt = DateTime.Now,
                    ProductId = Guid.NewGuid(),
                    ProductName = "novo_produto2",
                    Quantity = 113
                }
            };


        }
        [TestMethod, TestCategory("Application.Services")]
        public void MappingExtension_WishListItemDTO_UpdateWith_WishListItemModel()
        {
            WishListItem wishListItemModel = new WishListItem()
            {
                UpdatedAt = DateTime.Now,
                ProductId = Guid.NewGuid(),
                ProductName = "novo_produto",
                Quantity = 112
            };

            var wishListItemDTO = new Farfetch.Wishlist.Application.DTO.Response.WishListItem().UpdateWith(wishListItemModel);
            Assert.AreEqual(wishListItemDTO.Id, wishListItemModel.Id);
            Assert.AreEqual(wishListItemDTO.ProductId, wishListItemModel.ProductId);
            Assert.AreEqual(wishListItemDTO.ProductName, wishListItemModel.ProductName);
            Assert.AreEqual(wishListItemDTO.Quantity, wishListItemModel.Quantity);
            Assert.AreEqual(wishListItemDTO.UpdatedAt , wishListItemModel.UpdatedAt);
            Assert.AreEqual(wishListItemDTO.CreatedAt, wishListItemModel.CreatedAt);
        }

        [TestMethod, TestCategory("Application.Services")]
        public void MappingExtension_WishListDetails_UpdateWith_WishListModel()
        {
            WishList wishListModel = new WishList()
            {
                UserId = 1,
                TenantId = 1,
                Items = this.wishListItems
            };

            var wishListDetails = new Farfetch.Wishlist.Application.DTO.WishListDetails().UpdateWith(wishListModel);
            Assert.AreEqual(wishListDetails.Id, wishListModel.Id);
            Assert.AreEqual(wishListDetails.TenantId, wishListModel.TenantId);
            Assert.AreEqual(wishListDetails.UserId, wishListModel.UserId);
            Assert.AreEqual(wishListDetails.CreatedAt, wishListModel.CreatedAt);
        }

        [TestMethod, TestCategory("Application.Services")]
        public void MappingExtension_WishListDTO_UpdateWith_WishListModel()
        {
            WishList wishListModel = new WishList()
            {
                UserId = 1,
                TenantId = 1
            };

            var wishListDTO = new Farfetch.Wishlist.Application.DTO.Response.WishList().UpdateWith(wishListModel);


            Assert.AreEqual(wishListDTO.WishListDetails.Id, wishListModel.Id);
            Assert.AreEqual(wishListDTO.WishListDetails.TenantId, wishListModel.TenantId);
            Assert.AreEqual(wishListDTO.WishListDetails.UserId, wishListModel.UserId);
            Assert.AreEqual(wishListDTO.WishListDetails.CreatedAt, wishListModel.CreatedAt);

            Assert.IsTrue(wishListDTO.WishListItems.Select(e => new { e.Id, e.ProductId, e.ProductName, e.Quantity, e.UpdatedAt, e.CreatedAt })
                        .ToList()
                        .SequenceEqual(
                        wishListModel.Items.Select(e => new { e.Id, e.ProductId, e.ProductName, e.Quantity, e.UpdatedAt, e.CreatedAt })
                        .ToList()));
        }

        [TestMethod, TestCategory("Application.Services")]
        public void MappingExtension_ListOfWishListItemDTO_UpdateWith_ListOfWishListItemModel()
        {
            List<WishListItem> source = this.wishListItems;

            var target = new List<Farfetch.Wishlist.Application.DTO.Response.WishListItem>().UpdateWith(source);

            //adicionar asserts
            Assert.IsTrue(source.Select(e => new {e.Id,e.ProductId,e.ProductName,e.Quantity,e.UpdatedAt,e.CreatedAt})
                        .ToList()
                        .SequenceEqual(
                        target.Select(e => new { e.Id, e.ProductId, e.ProductName, e.Quantity, e.UpdatedAt, e.CreatedAt })
                        .ToList()));
        }
    }
}
