﻿using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Farfetch.Wishlist.Domain.Model;
using Farfetch.Wishlist.Domain.Core.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Farfetch.Wishlist.Application.Services.Exceptions;
using Farfetch.Wishlist.Application.Services.Implementation;
using Farfetch.Wishlist.Infrastructure.CrossCutting;

namespace Application.Services.Tests.Implementation
{
    [TestClass]
    public class WishListServiceTests
    {
        private Mock<IWishListRepository> wishListRepositoryMock;

        [TestInitialize]
        public void Initialize()
        {
            wishListRepositoryMock = new Mock<IWishListRepository>();
        }

        [TestMethod, TestCategory("Application.Services")]
        [ExpectedException(typeof(ResourceNotFoundException))]  
        public async Task GetWishList_ThrowsResourceNotFoundException()
        {
            this.wishListRepositoryMock.Setup(m => m.GetWishList(It.IsAny<Guid>()))
                .ReturnsAsync((WishList)null);
            var service = CreateWishListService();
            await service.GetWishList(Guid.NewGuid());
        }
        
        [TestMethod, TestCategory("Application.Services")]
        public async Task GetWishList_WishListMatch_ReturnsDTOWishList()
        {
            var guid = new Guid("ba5d3101-f716-499e-9644-e949d7811047");
            var wishList = new WishList() {
                TenantId = 110,
                UserId = 110,
                Items = new List<WishListItem>()
                {
                    new WishListItem()
                    {
                    ProductId = new Guid("da5d3101-f716-499e-9644-e949d7811047"),
                    ProductName = "product_4000",
                    Quantity = 2
                    }
                }
            };

            this.wishListRepositoryMock.Setup(m => m.GetWishList(It.IsAny<Guid>()))
                .ReturnsAsync(wishList);

            var service = CreateWishListService();
            var wishListNew = await service.GetWishList(guid);

            Assert.AreEqual(wishList.Id, wishListNew.WishListDetails.Id);
            Assert.AreEqual(wishList.UserId, wishListNew.WishListDetails.UserId);
            Assert.AreEqual(wishList.TenantId, wishListNew.WishListDetails.TenantId);
            Assert.IsNotNull(wishListNew.WishListItems);
            Assert.AreEqual(wishList.Items[0].Id, wishListNew.WishListItems[0].Id);
            Assert.AreEqual(wishList.Items[0].ProductId, wishListNew.WishListItems[0].ProductId);
            Assert.AreEqual(wishList.Items[0].ProductName, wishListNew.WishListItems[0].ProductName);
            Assert.AreEqual(wishList.Items[0].Quantity, wishListNew.WishListItems[0].Quantity);

        }

        [TestMethod, TestCategory("Application.Services")]
        public async Task CreateWishList_ReturnsCreatedDTOWishList()
        {
            var guid = Guid.NewGuid();
            var newWishList = new WishList()
            {
                TenantId = 111,
                UserId = 112
            };

            this.wishListRepositoryMock.Setup(m => m.InsertAsync(newWishList)).Returns(TaskHelpers.Completed);
            var service = CreateWishListService();
            var serviceNewWishList = await service.CreateWishList(111,112);

            Assert.IsFalse(serviceNewWishList.WishListItems.Any());
            Assert.AreEqual(newWishList.TenantId, serviceNewWishList.WishListDetails.TenantId);
            Assert.AreEqual(newWishList.UserId, serviceNewWishList.WishListDetails.UserId);


        }

        private WishListService CreateWishListService()
        {
            return new WishListService(this.wishListRepositoryMock.Object);
        }
    }
}
