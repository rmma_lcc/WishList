﻿using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Farfetch.Wishlist.Domain.Model;
using Farfetch.Wishlist.Domain.Core.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Farfetch.Wishlist.Application.Services.Exceptions;
using Farfetch.Wishlist.Application.Services.Implementation;
using Application.Kafka.Producers;
using Application.Kafka.Contracts;
using Farfetch.Framework.Kafka.Producer;

namespace Application.Services.Tests.Implementation
{
    
    [TestClass]
    public class WishListItemServiceTests
    {
        private Mock<IWishListRepository> wishListRepositoryMock;
        private Mock<IKafkaProducerFactory> kafkaProducerFactory;
        private Mock<IProducer<WishListItemMessage>> producer;

        [TestInitialize]
        public void Initialize()
        {
            wishListRepositoryMock = new Mock<IWishListRepository>();
            kafkaProducerFactory = new Mock<IKafkaProducerFactory>();
            producer = new Mock<IProducer<WishListItemMessage>>();
    }

        
        [TestMethod, TestCategory("Application.Services")]
        [ExpectedException(typeof(ResourceNotFoundException))]
        public async Task AddWishListItem_WishListItemNotFound_ThrowsResourceNotFoundException()
        {
            this.wishListRepositoryMock.Setup(m => m.GetWishList(It.IsAny<Guid>()))
                .ReturnsAsync((WishList)null);

            var service = CreateWishListItemService();
            await service.AddItem(Guid.NewGuid(), Guid.NewGuid(),"product_name",1);
        }

        [TestMethod, TestCategory("Application.Services")]
        [ExpectedException(typeof(Farfetch.Wishlist.Infrastructure.CrossCutting.Exceptions.AlreadyExistsException))]
        public async Task AddWishListItem_ProductIdFound_ThrowsAlreadyExistsResourceException()
        {
            var wishList = new WishList(111, 112)
            {
                Items = new List<WishListItem>()
                {
                    new WishListItem()
                    {
                        ProductId = new Guid("20554d6e-29bb-11e5-b345-feff819cdc6f"),
                        ProductName = "product_01",
                        Quantity = 1
                    }
                }
            };

            this.wishListRepositoryMock.Setup(m => m.GetWishList(It.IsAny<Guid>()))
                .ReturnsAsync(wishList);

            var service = CreateWishListItemService();
            await service.AddItem(Guid.NewGuid(), new Guid("20554d6e-29bb-11e5-b345-feff819cdc6f"), "product_name", 1);
        }

        [TestMethod, TestCategory("Application.Services")]
        public async Task AddWishListItem_ReturnsListOfDTOWishListItem()
        {
            var productId = new Guid("10554d6e-29bb-11e5-b345-feff819cdc6f");
            var wishList = new WishList(111, 112)
            {
                Items = new List<WishListItem>()
                {
                    new WishListItem()
                    {
                        ProductId = new Guid("20554d6e-29bb-11e5-b345-feff819cdc6f"),
                        ProductName = "product_01",
                        Quantity = 1
                    }
                }
            };

            this.wishListRepositoryMock.Setup(m => m.GetWishList(It.IsAny<Guid>()))
                .ReturnsAsync((wishList));
           
            var service = CreateWishListItemService();
            var serviceWishList = await service.AddItem(wishList.Id, productId, "product_name", 1);

            this.wishListRepositoryMock.Verify(m => m.UpdateAsync(It.IsAny<WishList>()), Times.Once);

            Assert.IsTrue(wishList.Items.Select(e => new {e.ProductId, e.ProductName, e.Quantity })
                        .ToList()
                        .SequenceEqual(
                        serviceWishList.Select(e => new { e.ProductId, e.ProductName, e.Quantity })
                        .ToList()));
        }
        
       
        
        [TestMethod, TestCategory("Application.Services")]
        [ExpectedException(typeof(ResourceNotFoundException))]
        public async Task UpdateWishListItem_WishListNotFound_ThrowsResourceNotFoundException()
        {
            this.wishListRepositoryMock.Setup(m => m.GetWishList(It.IsAny<Guid>()))
                .ReturnsAsync((WishList)null);

            var service = CreateWishListItemService();
            await service.UpdateItem(Guid.NewGuid(), Guid.NewGuid(), 1);
        }

        [TestMethod, TestCategory("Application.Services")]
        [ExpectedException(typeof(Farfetch.Wishlist.Infrastructure.CrossCutting.Exceptions.ResourceNotFoundException))]
        public async Task UpdateWishListItem_WishListItemNotFound_ThrowsResourceNotFoundException()
        {
            var wishList = new WishList(111, 112);

            this.wishListRepositoryMock.Setup(m => m.GetWishList(It.IsAny<Guid>()))
                .ReturnsAsync(wishList);

            var service = CreateWishListItemService();
            await service.UpdateItem(Guid.NewGuid(), Guid.NewGuid(), 1);
        }

        
        [TestMethod, TestCategory("Application.Services")]
        public async Task UpdateWishListItem_WishListItemFound()
        {
            var wishListExpected = new WishList(111, 112)
            {
                Items = new List<WishListItem>()
                {
                    new WishListItem()
                    {
                        ProductId = new Guid("20554d6e-29bb-11e5-b345-feff819cdc6f"),
                        ProductName = "product_01",
                        Quantity = 1
                    }
                }
            };

            var wishListAfterUpdate = new WishList(111, 112)
            {
                Id = wishListExpected.Id,
                CreatedAt = wishListExpected.CreatedAt,
                Items = new List<WishListItem>(wishListExpected.Items)
            };
            wishListAfterUpdate.Items[0].Quantity = 2;

            this.wishListRepositoryMock.Setup(m => m.GetWishList(It.IsAny<Guid>())).ReturnsAsync(wishListExpected);

            var service = CreateWishListItemService();
            var wishListItemUpdated = await service.UpdateItem(wishListExpected.Id, wishListAfterUpdate.Items[0].Id, 2);

            this.wishListRepositoryMock.Verify(m => m.UpdateAsync(It.IsAny<WishList>()),Times.Once);

            Assert.IsTrue(wishListAfterUpdate.Items.Select(e => new { e.ProductId, e.ProductName, e.Quantity })
                        .ToList()
                        .SequenceEqual(
                        wishListItemUpdated.Select(e => new { e.ProductId, e.ProductName, e.Quantity })
                        .ToList()));
        }


        [TestMethod, TestCategory("Application.Services")]
        [ExpectedException(typeof(ResourceNotFoundException))]
        public async Task DeleteWishListItem_WishListNotFound_ThrowsResourceNotFoundException()
        {
            this.wishListRepositoryMock.Setup(m => m.GetWishList(It.IsAny<Guid>()))
                .ReturnsAsync((WishList)null);

            var service = CreateWishListItemService();
            await service.DeleteItem(Guid.NewGuid(), Guid.NewGuid());
        }

        [TestMethod, TestCategory("Application.Services")]
        [ExpectedException(typeof(Farfetch.Wishlist.Infrastructure.CrossCutting.Exceptions.ResourceNotFoundException))]
        public async Task DeleteWishListItem_WishListItemNotFound_ThrowsResourceNotFoundException()
        {
            var wishList = new WishList(111, 112);

            this.wishListRepositoryMock.Setup(m => m.GetWishList(It.IsAny<Guid>()))
                .ReturnsAsync(wishList);

            var service = CreateWishListItemService();
            await service.DeleteItem(Guid.NewGuid(), Guid.NewGuid());
        }


        [TestMethod, TestCategory("Application.Services")]
        public async Task DeleteWishListItem_WishListItemFound()
        {
            var wishListExpected = new WishList(111, 112)
            {
                Items = new List<WishListItem>()
                {
                    new WishListItem()
                    {
                        ProductId = new Guid("20554d6e-29bb-11e5-b345-feff819cdc6f"),
                        ProductName = "product_01",
                        Quantity = 1
                    }
                }
            };

            var wishListAfterDelete = new WishList(111, 112)
            {
                Id = wishListExpected.Id,
                CreatedAt = wishListExpected.CreatedAt,
                Items = new List<WishListItem>()
            };

            this.wishListRepositoryMock.Setup(m => m.GetWishList(It.IsAny<Guid>())).ReturnsAsync(wishListExpected);

            var service = CreateWishListItemService();
            await service.DeleteItem(wishListExpected.Id, wishListExpected.Items[0].Id);

            this.wishListRepositoryMock.Verify(m => m.UpdateAsync(It.IsAny<WishList>()), Times.Once);

            Assert.AreEqual(wishListAfterDelete.Items, wishListAfterDelete.Items);
        }
       
        private WishListItemService CreateWishListItemService()
        {
            var service = new WishListItemService(this.wishListRepositoryMock.Object, this.kafkaProducerFactory.Object);
            service.Producer = producer.Object;
            return service;
        }
        
    }
}
