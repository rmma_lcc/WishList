﻿namespace Application.Services.Tests.Handlers
{
    using Moq;
    using System;
    using Application.Kafka.Contracts;
    using Farfetch.Framework.Kafka.Producer;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Farfetch.Wishlist.Application.Services.Handlers;
    using Farfetch.Wishlist.Domain.Model;

    [TestClass]
    public class EventsHandlerTests
    {
        private Mock<IProducer<WishListItemMessage>> producer;
        private EventsHandler events;

        [TestInitialize]
        public void Initialize()
        {
            producer = new Mock<IProducer<WishListItemMessage>>();
            events = new EventsHandler(producer.Object);
        }

        [TestMethod]
        [TestCategory("Application.Services")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void EventsHandler_NullProducer_ThrowArgumentNullException()
        {
            events = new EventsHandler(null);
        }

        [TestMethod]
        [TestCategory("Application.Services")]
        [ExpectedException(typeof(ArgumentNullException))]
        public async void WishListItemAddedHandler_NullProducer_ThrowArgumentNullException()
        {
           await events.WishListItemAddedHandler(null);
        }

        [TestMethod]
        [TestCategory("Application.Services")]
        [ExpectedException(typeof(ArgumentNullException))]
        public async void WishListItemAddedHandler_ProduceMessage()
        {
            WishListItem wishListItem = new WishListItem()
            {
                Id = Guid.NewGuid(),
                ProductId = Guid.NewGuid(),
                ProductName = "product_name",
                Quantity = 1,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt =DateTime.UtcNow
            };

            await events.WishListItemAddedHandler(wishListItem);

            producer.Verify(p => p.ProduceAsync(It.IsAny<WishListItemMessage>()), Times.Once);
        }

        [TestMethod]
        [TestCategory("Application.Services")]
        [ExpectedException(typeof(ArgumentNullException))]
        public async void WishListItemUploadedHandler_NullProducer_ThrowArgumentNullException()
        {
            await events.WishListItemAddedHandler(null);
        }

        [TestMethod]
        [TestCategory("Application.Services")]
        [ExpectedException(typeof(ArgumentNullException))]
        public async void WishListItemUpdatedHandler_ProduceMessage()
        {
            WishListItem wishListItem = new WishListItem()
            {
                Id = Guid.NewGuid(),
                ProductId = Guid.NewGuid(),
                ProductName = "product_name",
                Quantity = 1,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow
            };

            await events.WishListItemDeletedHandler(wishListItem);

            producer.Verify(p => p.ProduceAsync(It.IsAny<WishListItemMessage>()), Times.Once);
        }

        [TestMethod]
        [TestCategory("Application.Services")]
        [ExpectedException(typeof(ArgumentNullException))]
        public async void WishListItemDeletedHandler_NullProducer_ThrowArgumentNullException()
        {
            await events.WishListItemAddedHandler(null);
        }

        [TestMethod]
        [TestCategory("Application.Services")]
        [ExpectedException(typeof(ArgumentNullException))]
        public async void WishListItemDeletedHandler_ProduceMessage()
        {
            WishListItem wishListItem = new WishListItem()
            {
                Id = Guid.NewGuid(),
                ProductId = Guid.NewGuid(),
                ProductName = "product_name",
                Quantity = 1,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow
            };

            await events.WishListItemDeletedHandler(wishListItem);

            producer.Verify(p => p.ProduceAsync(It.IsAny<WishListItemMessage>()), Times.Once);
        }
    }
}
