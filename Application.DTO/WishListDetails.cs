﻿namespace Farfetch.Wishlist.Application.DTO
{
    using System;
    public class WishListDetails
    {
        public Guid Id { get; set; }
        public int UserId { get; set; }
        public int TenantId { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
