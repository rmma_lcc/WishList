﻿namespace Farfetch.Wishlist.Application.DTO.Response
{
    using System.Collections.Generic;
    public class WishList
    {
        public WishListDetails WishListDetails { get; set; } 
        public IList<WishListItem> WishListItems { get; set; }
    }
}
