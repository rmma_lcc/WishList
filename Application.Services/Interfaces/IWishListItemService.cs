﻿namespace Farfetch.Wishlist.Application.Services.Interfaces
{
    using System;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using Farfetch.Wishlist.Application.DTO.Response;

    public interface IWishListItemService
    {
        
        Task<IList<WishListItem>> AddItem(Guid wishListId, Guid productId, string productName,int quantity);

        Task<IList<WishListItem>> UpdateItem(Guid wishListId, Guid wishListItemId, int quantity);

        Task DeleteItem(Guid wishListId, Guid wishListItemId);
    }
}
