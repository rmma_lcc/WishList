﻿namespace Farfetch.Wishlist.Application.Services.Interfaces
{
    using System;
    using System.Threading.Tasks;
    using Farfetch.Wishlist.Application.DTO.Response;

    public interface IWishListService
    {
        
        Task<WishList> GetWishList(Guid wishListId);
        Task<WishList> CreateWishList(int tenantId, int userId);
        
    }
}
