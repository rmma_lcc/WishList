﻿namespace Farfetch.Wishlist.Application.Services.Exceptions
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class AlreadyExistsResourceException : Exception
    {
        public AlreadyExistsResourceException()
        {
        }

        public AlreadyExistsResourceException(string message)
            : base(message)
        {
        }

        public AlreadyExistsResourceException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
