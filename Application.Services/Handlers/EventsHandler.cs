﻿namespace Farfetch.Wishlist.Application.Services.Handlers
{
    using System;
    using System.Threading.Tasks;
    using Farfetch.Wishlist.Domain.Model;
    using Farfetch.Framework.Kafka.Producer;
    using global::Application.Kafka.Contracts;
    using Farfetch.Wishlist.Infrastructure.CrossCutting;
    using Farfetch.Wishlist.Infrastructure.CrossCutting.Extensions;

    public class EventsHandler
    {
        private readonly IProducer<WishListItemMessage> producer;

        public EventsHandler(IProducer<WishListItemMessage> producer)
        {
            Guard.Against<ArgumentNullException>(producer.IsNull(), "Producer is null.");
            this.producer = producer;
        }

        public async Task WishListItemAddedHandler(WishListItem wishlist)
        {
            Guard.Against<ArgumentNullException>(producer.IsNull(), "Producer is null.");
            await producer.ProduceAsync(new WishListItemAddedMessage(wishlist));
        }

        public async Task WishListItemUpdatedHandler(WishListItem wishlist,int previousQuantity)
        {
            Guard.Against<ArgumentNullException>(producer.IsNull(), "Producer is null.");
            await producer.ProduceAsync(new WishListItemUpdatedMessage(wishlist, previousQuantity));
        }

        public async Task WishListItemDeletedHandler(WishListItem wishlist)
        {
            Guard.Against<ArgumentNullException>(producer.IsNull(), "Producer is null.");
            await producer.ProduceAsync(new WishListItemDeletedMessage(wishlist));
        }
    }
}
