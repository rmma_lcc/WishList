﻿using System.Collections.Generic;

namespace Farfetch.Wishlist.Application.Services.Extensions
{
    public static class MappingExtensions
    {
        public static DTO.Response.WishListItem UpdateWith(this DTO.Response.WishListItem target, Domain.Model.WishListItem source)
        {
            target.Id = source.Id;
            target.ProductId = source.ProductId;
            target.ProductName = source.ProductName;
            target.Quantity = source.Quantity;
            target.CreatedAt = source.CreatedAt;
            target.UpdatedAt = source.UpdatedAt;

            return target;
        }

        public static DTO.WishListDetails UpdateWith(this DTO.WishListDetails target, Domain.Model.WishList source)
        {
            target.Id = source.Id;
            target.TenantId = source.TenantId;
            target.UserId = source.UserId;
            target.CreatedAt = source.CreatedAt;

            return target;
        }

        public static DTO.Response.WishList UpdateWith(this DTO.Response.WishList target, Domain.Model.WishList source)
        {
            target.WishListDetails = new DTO.WishListDetails().UpdateWith(source);
            target.WishListItems = new List<DTO.Response.WishListItem>();

            foreach(var item in source.Items)
            {
                target.WishListItems.Add(new DTO.Response.WishListItem().UpdateWith(item));
            }

            return target;
        }

        public static IList<DTO.Response.WishListItem> UpdateWith(this List<DTO.Response.WishListItem> target, List<Domain.Model.WishListItem> source)
        {
            foreach (var item in source)
            {
                target.Add(new DTO.Response.WishListItem().UpdateWith(item));
            }

            return target;
        }
    }
}
