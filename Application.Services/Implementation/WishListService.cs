﻿namespace Farfetch.Wishlist.Application.Services.Implementation
{
    using System;
    using System.Threading.Tasks;
    using Farfetch.Wishlist.Domain.Model;
    using Farfetch.Wishlist.Domain.Core.Repository;
    using Farfetch.Wishlist.Infrastructure.CrossCutting;
    using Farfetch.Wishlist.Application.Services.Extensions;
    using Farfetch.Wishlist.Application.Services.Interfaces;
    using Farfetch.Wishlist.Application.Services.Exceptions;
    using Farfetch.Wishlist.Infrastructure.CrossCutting.Extensions;

    public class WishListService : IWishListService
    {
        private readonly IWishListRepository wishListRepository;

        public WishListService(IWishListRepository wishListRepository)
        {
            this.wishListRepository = wishListRepository;
        }
        
        public async Task<DTO.Response.WishList> GetWishList(Guid wishListId)
        {
            var wishList = await this.wishListRepository.GetWishList(wishListId);
            Guard.Against<ResourceNotFoundException>(wishList.IsNull(),"WishList not found.");

            return new DTO.Response.WishList().UpdateWith(wishList);
        }
        
        public async Task<DTO.Response.WishList> CreateWishList(int tenantId, int userId)
        {
            var wishList = new WishList(tenantId, userId);

            await this.wishListRepository.InsertAsync(wishList);

            return new DTO.Response.WishList().UpdateWith(wishList);
        }
        
    }
}
