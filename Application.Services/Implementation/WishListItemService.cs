﻿namespace Farfetch.Wishlist.Application.Services.Implementation
{
    using System;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using Application.Services.Handlers;
    using Farfetch.Framework.Kafka.Producer;
    using global::Application.Kafka.Contracts;
    using global::Application.Kafka.Producers;
    using Farfetch.Wishlist.Domain.Core.Repository;
    using Farfetch.Wishlist.Application.DTO.Response;
    using Farfetch.Wishlist.Infrastructure.CrossCutting;
    using Farfetch.Wishlist.Application.Services.Interfaces;
    using Farfetch.Wishlist.Application.Services.Extensions;
    using Farfetch.Wishlist.Application.Services.Exceptions;
    using Farfetch.Wishlist.Infrastructure.CrossCutting.Extensions;


    public class WishListItemService : IWishListItemService
    {
        private readonly IWishListRepository wishListRepository;
        private readonly IKafkaProducerFactory kafkaProducerFactory;
        public WishListItemService( IWishListRepository wishListRepository, IKafkaProducerFactory kafkaProducerFactory)
        {
            this.wishListRepository = wishListRepository;
            this.kafkaProducerFactory = kafkaProducerFactory;
            Producer = kafkaProducerFactory.Create<WishListItemMessage>();
        }

        public IProducer<WishListItemMessage> Producer { get; set; }

        public EventsHandler Events => new EventsHandler(Producer);

        public async Task<IList<WishListItem>> AddItem(Guid wishListId, Guid productId, string productName, int quantity)
        {

            var wishList = await this.wishListRepository.GetWishList(wishListId);
            Guard.Against<ResourceNotFoundException>(wishList.IsNull(), "WishList not found.");

            var wishListItem = wishList.AddItem(productId, productName, quantity);

            await this.wishListRepository.UpdateAsync(wishList);

            await Events.WishListItemAddedHandler(wishListItem);

            return new List<WishListItem>().UpdateWith(wishList.Items);
        }

        public async Task<IList<WishListItem>> UpdateItem(Guid wishListId, Guid wishListItemId, int quantity)
        {
            var wishList = await this.wishListRepository.GetWishList(wishListId);
            Guard.Against<ResourceNotFoundException>(wishList.IsNull(), "WishList not found.");

            Tuple <Domain.Model.WishListItem,int> result = wishList.UpdateItem(wishListItemId,quantity);

            await this.wishListRepository.UpdateAsync(wishList);

            await Events.WishListItemUpdatedHandler(result.Item1,result.Item2);

            return new List<WishListItem>().UpdateWith(wishList.Items);
        }

        public async Task DeleteItem(Guid wishListId, Guid wishListItemId)
        {
            var wishList = await this.wishListRepository.GetWishList(wishListId);
            Guard.Against<ResourceNotFoundException>(wishList.IsNull(), "WishList not found.");

            var wishListItem = wishList.DeleteItem(wishListItemId);

            await this.wishListRepository.UpdateAsync(wishList);

            await Events.WishListItemDeletedHandler(wishListItem);

            wishListItem.UpdatedAt = DateTime.UtcNow;
        }
    }
}
