﻿namespace Farfetch.Wishlist.Infrastructure.CrossCutting.Configuration
{
    using System;
    using System.IO;
    using Newtonsoft.Json;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class JsonConfiguration
    {
        public static T Deserialize<T>(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath))
            {
                throw new ArgumentNullException(nameof(filePath), "File Path cannot be null or empty.");
            }

            T obj;

            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException($"File {filePath} doesn't exist.");
            }

            using (var file = File.OpenText(filePath))
            {
                var serializer = new JsonSerializer();
                obj = (T)serializer.Deserialize(file, typeof(T));
            }

            return obj;
        }
    }
}
