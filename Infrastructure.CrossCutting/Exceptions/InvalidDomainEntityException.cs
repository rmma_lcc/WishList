﻿namespace Farfetch.Wishlist.Infrastructure.CrossCutting.Exceptions
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class InvalidDomainEntityException : Exception
    {
        public InvalidDomainEntityException()
        {
        }

        public InvalidDomainEntityException(string message)
            : base(message)
        {
        }

        public InvalidDomainEntityException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
