using Moq;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Presentation.WebAPI.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Farfetch.Wishlist.Application.Services.Interfaces;


namespace Presentation.WebAPI.Tests
{
    
    [TestClass]
    public class WishListControllerTests
    {
        private Mock<IWishListService> wishListMock;

        [TestInitialize]
        public void Initialize()
        {
            wishListMock = new Mock<IWishListService>();
        }

        [TestMethod, TestCategory("Presentation.WebAPI")]
        public async Task WishListController_GetWishList()
        {
            var expectedResult = new Farfetch.Wishlist.Application.DTO.Response.WishList();
            var wishListId = It.IsAny<Guid>();
            wishListMock.Setup(e => e.GetWishList(wishListId)).ReturnsAsync(expectedResult);

            var controller = new WishListController(wishListMock.Object);
            var result = await controller.GetWishList(wishListId) as OkObjectResult;

            wishListMock.Verify(e => e.GetWishList(wishListId), Times.Once);
            Assert.AreSame(expectedResult, result.Value);
        }

        [TestMethod, TestCategory("Presentation.WebAPI")]
        public async Task WishListController_CreateWishList()
        {
            var expectedResult = new Farfetch.Wishlist.Application.DTO.Response.WishList();
            int tenantId = It.IsAny<Int32>();
            int userId = It.IsAny<Int32>();

            wishListMock.Setup(e => e.CreateWishList(tenantId, userId)).ReturnsAsync(expectedResult);

            var controller = new WishListController(wishListMock.Object);
            var result = await controller.CreateWishList(tenantId, userId) as OkObjectResult;

            wishListMock.Verify(e => e.CreateWishList(tenantId, userId), Times.Once);
            Assert.AreSame(expectedResult, result.Value);
        }
    }
}
