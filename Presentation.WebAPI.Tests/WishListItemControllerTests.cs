﻿namespace Presentation.WebAPI.Tests.Controllers
{
    using Moq;
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using Presentation.WebAPI.Controllers;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Farfetch.Wishlist.Infrastructure.CrossCutting;
    using Farfetch.Wishlist.Application.Services.Interfaces;

    [TestClass]
    public class WishListItemControllerTests
    {
        private Mock<IWishListItemService> wishListItemMock;
        [TestInitialize]
        public void Initialize()
        {
            wishListItemMock = new Mock<IWishListItemService>();
        }

        [TestMethod, TestCategory("Presentation.WebAPI")]
        public async Task WishListItemController_AddItem()
        {
            var expectedResult = new List<Farfetch.Wishlist.Application.DTO.Response.WishListItem>();
            var wishListId = It.IsAny<Guid>();
            var productId = It.IsAny<Guid>();
            var productName = It.IsAny<string>();
            var quantity = It.IsAny<Int32>();

            wishListItemMock.Setup(e => e.AddItem(wishListId, productId, productName, quantity)).ReturnsAsync(expectedResult);

            var controller = new WishListItemController(wishListItemMock.Object);
            var result = await controller.AddItem(wishListId, productId, productName, quantity) as OkObjectResult;

            wishListItemMock.Verify(e => e.AddItem(wishListId, productId, productName, quantity), Times.Once);
            Assert.AreSame(expectedResult, result.Value);
        }
        
        [TestMethod, TestCategory("Presentation.WebAPI")]
        public async Task WishListItemController_UpdateItem()
        {
            var expectedResult = new List<Farfetch.Wishlist.Application.DTO.Response.WishListItem>();
            var wishListId = It.IsAny<Guid>();
            var wishListItemId = It.IsAny<Guid>();
            var quantity = It.IsAny<Int32>();

            wishListItemMock.Setup(e => e.UpdateItem(wishListId, wishListItemId, quantity)).ReturnsAsync(expectedResult);

            var controller = new WishListItemController(wishListItemMock.Object);
            var result = await controller.UpdateItem(wishListId, wishListItemId, quantity) as OkObjectResult;

            wishListItemMock.Verify(e => e.UpdateItem(wishListId, wishListItemId, quantity), Times.Once);
            Assert.AreSame(expectedResult, result.Value);
        }

        [TestMethod, TestCategory("Presentation.WebAPI")]
        public async Task WishListItemController_DeleteItem()
        {
            var wishListId = It.IsAny<Guid>();
            var wishListItemId = It.IsAny<Guid>();

            wishListItemMock.Setup(e => e.DeleteItem(wishListId, wishListItemId)).Returns(TaskHelpers.Completed());

            var controller = new WishListItemController(wishListItemMock.Object);
            var result = await controller.DeleteItem(wishListId, wishListItemId);

            wishListItemMock.Verify(e => e.DeleteItem(wishListId, wishListItemId), Times.Once);
        }
        
    }
}
