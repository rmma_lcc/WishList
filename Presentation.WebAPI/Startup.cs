﻿namespace Presentation.WebAPI
{
    using Cassandra.Mapping;
    using Application.Kafka;
    using Application.Kafka.Producers;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Application.Kafka.Interfaces;
    using Swashbuckle.AspNetCore.Swagger;
    using Presentation.WebAPI.Extensions;
    using System.Diagnostics.CodeAnalysis;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Farfetch.Wishlist.Domain.Core.Repository;
    using Farfetch.Wishlist.Data.Repository.CassandraDB;
    using Farfetch.Wishlist.Data.Repository.Implementation;
    using Farfetch.Wishlist.Application.Services.Interfaces;
    using Farfetch.Wishlist.Application.Services.Implementation;
    using Farfetch.Wishlist.Data.Repository.Implementation.Mapping;



    [ExcludeFromCodeCoverage]
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            MappingConfiguration.Global.Define<ConfiguredMappings>();
            services.AddSingleton<IWishListRepository, WishListRepository>();
            services.AddSingleton<IWishListService, WishListService>();
            services.AddSingleton<IWishListItemService, WishListItemService>();
            services.AddSingleton<ICassandraConfiguration>(ConfiguredMappings.CassandraConfigurationFactory);
            services.AddSingleton<ICassandraConnection, CassandraConnection>();
            services.AddSingleton<IKafkaConfiguration, KafkaConfiguration>();
            services.AddSingleton<IKafkaProducerFactory, KafkaProducerFactory>();
            services.Configure<ReadKafkaConfiguration>(Configuration.GetSection("KafkaConfiguration"));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseMiddleware(typeof(GlobalExceptionFilter));

            app.UseMvc();

            app.UseMvcWithDefaultRoute();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
        }
    }
}
