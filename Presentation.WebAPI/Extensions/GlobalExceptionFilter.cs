﻿namespace Presentation.WebAPI.Extensions
{
    using System;
    using System.Net;
    using Newtonsoft.Json;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Farfetch.Framework.Rest.Common;
    using System.Diagnostics.CodeAnalysis;
    using Farfetch.Wishlist.Infrastructure.CrossCutting.Exceptions;
    

    [ExcludeFromCodeCoverage]
    public class GlobalExceptionFilter 
    {
        private readonly RequestDelegate next;

        public GlobalExceptionFilter(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context /* other scoped dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var code = HttpStatusCode.InternalServerError; // 500 if unexpected
            var errorCode = ErrorCodes.InternalServerError;
            var message = string.Empty;
            if (exception is Farfetch.Wishlist.Application.Services.Exceptions.ResourceNotFoundException) { errorCode = ErrorCodes.Notfound; code = HttpStatusCode.NotFound; }
            else if (exception is ResourceNotFoundException) { errorCode = ErrorCodes.Notfound; code = HttpStatusCode.NotFound; }
            else if (exception is AlreadyExistsException) { errorCode = ErrorCodes.AlreadyExists; code = HttpStatusCode.Conflict; }
            else if (exception is InvalidDomainEntityException) { errorCode = ErrorCodes.ArgumentError; code = HttpStatusCode.BadRequest; }
            else if (exception is Farfetch.Wishlist.Application.Services.Exceptions.AlreadyExistsResourceException) { errorCode = ErrorCodes.AlreadyExists; code = HttpStatusCode.Conflict; }

            var result = JsonConvert.SerializeObject(new ApiError(exception.Message, errorCode, message));
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }
    }
}
