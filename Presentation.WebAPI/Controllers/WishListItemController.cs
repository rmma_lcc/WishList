﻿namespace Presentation.WebAPI.Controllers
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using Farfetch.Wishlist.Application.DTO.Response;
    using Farfetch.Wishlist.Application.Services.Interfaces;

    [Route("WishListItem")]
    public class WishListItemController : Controller
    {
        private readonly IWishListItemService wishListItemService;

        public WishListItemController(IWishListItemService wishListItemService)
        {
            this.wishListItemService = wishListItemService;
        }
        
        // POST
        [HttpPost, Produces(typeof(WishListItem))]
        [Route("{wishlistId}/WishListItems")]
        public async Task<IActionResult> AddItem(Guid wishListId, Guid productId, string productName, int quantity)
        {
            return Ok(await this.wishListItemService.AddItem(wishListId, productId, productName, quantity));
        }
        
        // PUT
        [HttpPut, Produces(typeof(WishListItem))]
        [Route ("{wishlistId}/WishListItems/{wishlistitemId}")]
        public async Task<IActionResult> UpdateItem(Guid wishListId, Guid wishListItemId, int quantity)
        {
            return Ok(await this.wishListItemService.UpdateItem(wishListId, wishListItemId, quantity));
        }

        
        // DELETE
        [HttpDelete, Produces(typeof(List<WishListItem>))]
        [Route("{wishlistId}/WishListItems/{wishlistitemId}")]
        public async Task<IActionResult> DeleteItem(Guid wishListId, Guid wishListItemId)
        {
            await this.wishListItemService.DeleteItem(wishListId, wishListItemId);
            return Ok(); 
        }
    }
}