﻿namespace Presentation.WebAPI.Controllers
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Farfetch.Wishlist.Application.DTO.Response;
    using Farfetch.Wishlist.Application.Services.Interfaces;

    [Route("WishList")]
    public class WishListController : Controller
    {
        private readonly IWishListService wishListService;

        public WishListController(IWishListService wishListService)
        {
            this.wishListService = wishListService;
    }
        
        [HttpGet, Produces((typeof(WishList)))]
        [Route("{wishListId}")]
        public async Task<IActionResult> GetWishList(Guid wishListId)
        {
            return Ok(await this.wishListService.GetWishList(wishListId));
        }
        

        // POST
        [HttpPost, Produces((typeof(WishList)))]
        public async Task<IActionResult> CreateWishList(int tenantId, int userId)
        {
            return Ok(await this.wishListService.CreateWishList(tenantId, userId));
        }
        
    }
}