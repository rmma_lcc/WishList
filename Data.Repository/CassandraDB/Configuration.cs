﻿namespace Farfetch.Wishlist.Data.Repository.CassandraDB
{
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class Configuration
    {
        public string ContactPoints { get; set; }

        public string KeySpace { get; set; }

        public int Timeout { get; set; }
    }
}
