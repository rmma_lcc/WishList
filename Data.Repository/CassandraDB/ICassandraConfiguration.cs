﻿namespace Farfetch.Wishlist.Data.Repository.CassandraDB
{
    public interface ICassandraConfiguration
    {
        Configuration Configuration { get; set; }
    }
}
