﻿namespace Farfetch.Wishlist.Data.Repository.CassandraDB
{
    using System;
    using System.IO;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class CassandraConfiguration : ICassandraConfiguration
    {
        public Configuration Configuration { get; set; }

        public static string GetFile() =>
            Path.GetFullPath($"{AppDomain.CurrentDomain.BaseDirectory}/Config/CassandraConfiguration.json");
    }
}
