﻿namespace Farfetch.Wishlist.Data.Repository.CassandraDB
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Farfetch.Framework.CassandraDriver;
    using Farfetch.Wishlist.Infrastructure.CrossCutting.Extensions;

    

    [ExcludeFromCodeCoverage]
    public class CassandraConnection : ICassandraConnection
    {
        private readonly ICassandraConfiguration configuration;

        private static bool cassandraKeyspaceCreated { get; set; }

        public Connection Connection { get; }

        public CassandraConnection(ICassandraConfiguration configuration)
        {
            this.configuration = configuration;

            var clusterBuilder = new ClusterBuilder()
                .WithDefaultKeySpace(this.configuration.Configuration.KeySpace)
                .AddContactPoints(SplitContactPointsIntoArray(this.configuration.Configuration.ContactPoints))
                .WithQueryTimeout(this.configuration.Configuration.Timeout);

            this.Connection = new Connection(clusterBuilder);

            
        }

        private static string[] SplitContactPointsIntoArray(string contactPoints)
        {
            return contactPoints.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        }

        public void Connect()
        {
            try
            {
                this.Connection.Connect();
                UserDefinedTypes.Register(this.Connection.Session);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to connect to cassandra."+ex.ToString());
                throw;
            }
        }

        public bool IsDisposed()
        {
            return this.Connection.Session.IsNull() ||
                this.Connection.Session.IsDisposed;
        }
    }
}
