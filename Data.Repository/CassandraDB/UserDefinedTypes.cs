﻿using Cassandra;
using System;
using System.Collections.Generic;
using System.Text;

namespace Farfetch.Wishlist.Data.Repository.CassandraDB
{
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public static class UserDefinedTypes
    {

        public static void Register(ISession session)
        {
            RegisterWishListItems(session);
        }

        private static void RegisterWishListItems(ISession session)
        {
            session.UserDefinedTypes.Define(
                UdtMap.For<Models.WishListItem>("udt_wishlistitem")
                    .Map(g => g.Id, "wishlistitem_id")
                    .Map(g => g.ProductId, "product_id")
                    .Map(g => g.ProductName, "product_name")
                    .Map(g => g.Quantity, "quantity")
                    .Map(g => g.CreatedAt, "created_at")
                    .Map(g => g.UpdatedAt, "update_at")
            );
        }
    }
}
