﻿namespace Farfetch.Wishlist.Data.Repository.CassandraDB
{
    using Farfetch.Framework.CassandraDriver;

    public interface ICassandraConnection
    {
        Connection Connection { get; }

        bool IsDisposed();

        void Connect();
    }
}
