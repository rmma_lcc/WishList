﻿namespace Farfetch.Wishlist.Data.Repository.Implementation.Mapping
{
    using System;
    using Cassandra.Mapping;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using Farfetch.Wishlist.Data.Repository.Models;
    using Farfetch.Wishlist.Data.Repository.CassandraDB;
    using Farfetch.Wishlist.Infrastructure.CrossCutting.Configuration;
    

    [ExcludeFromCodeCoverage]
    public class ConfiguredMappings : Mappings
    {
        public ConfiguredMappings() {
            For<WishList>()
           .TableName("wishlist")
           .PartitionKey("wishlist_id")
           .Column(u => u.Id, cm => cm.WithName("wishlist_id"))
           .Column(u => u.CreatedAt, cm => cm.WithName("created_at"))
           .Column(u => u.TenantId, cm => cm.WithName("tenant_id"))
           .Column(u => u.UserId, cm => cm.WithName("user_id"))
           .Column(u => u.Items, cm => cm.WithName("items").WithDbType<Dictionary<Guid, WishListItem>>().WithFrozenValue())
           .ExplicitColumns();

        }

        public static CassandraConfiguration CassandraConfigurationFactory =>
           JsonConfiguration.Deserialize<CassandraConfiguration>(CassandraConfiguration.GetFile());
    }
}
