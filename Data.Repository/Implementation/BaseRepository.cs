﻿namespace Farfetch.Wishlist.Data.Repository.Implementation
{
    using Cassandra;
    using Cassandra.Mapping;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using Farfetch.Framework.CassandraDriver;
    using Farfetch.Wishlist.Data.Repository.CassandraDB;

    

    [ExcludeFromCodeCoverage]
    public class BaseRepository
    {
        private ICassandraConnection cassandraConnection;

        protected static CqlQueryOptions CqlQueryOptions = new CqlQueryOptions()
           .SetConsistencyLevel(ConsistencyLevel.LocalQuorum)
           .DoNotPrepare();

        public BaseRepository(
            ICassandraConnection cassandraConnection)
        {
            this.cassandraConnection = cassandraConnection;
        }

        protected ISession Session => this.Connection.Session;

        protected IMapper Mapper => this.Connection.Mapper;

        protected Connection Connection => this.RefreshConnection();

        protected virtual async Task<IEnumerable<T>> FetchAsync<T>()
            where T : class
        {
            var fetchResult = await this.Mapper
                .FetchAsync<T>()
                .ConfigureAwait(false);

            return fetchResult;
        }

        protected virtual async Task<IEnumerable<T>> FetchAsync<T>(Cql query)
            where T : class
        {
            var fetchResult = await this.Mapper
                .FetchAsync<T>(query)
                .ConfigureAwait(false);

            return fetchResult;
        }

        protected virtual async Task<T> SingleAsync<T>(Cql query)
            where T : class
        {
            var fetchResult = await this.Mapper
                .SingleOrDefaultAsync<T>(query)
                .ConfigureAwait(false);

            return fetchResult;
        }

        protected async Task UpdateAsync<T>(T entity)
            where T : class
        {
            await this.Mapper.UpdateAsync(entity, CqlQueryOptions);
        }

        protected async Task InsertAsync<T>(T entity)
        {
            await this.Mapper.InsertAsync(entity, CqlQueryOptions);
        }

        protected async Task DeleteAsync<T>(T entity)
        {
            await this.Mapper.DeleteAsync(entity, CqlQueryOptions);
        }

        private Connection RefreshConnection()
        {
            
            this.cassandraConnection.Connect();
            //UserDefinedTypes.Register(this.Session);
            return this.cassandraConnection.Connection;
        }
    }
}
