﻿namespace Farfetch.Wishlist.Data.Repository.Implementation
{
    using System;
    using Cassandra.Mapping;
    using System.Threading.Tasks;
    using Farfetch.Wishlist.Domain.Model;
    using System.Diagnostics.CodeAnalysis;
    using Farfetch.Wishlist.Domain.Core.Repository;
    using Farfetch.Wishlist.Data.Repository.Extensions;
    using Farfetch.Wishlist.Data.Repository.CassandraDB;
    

    [ExcludeFromCodeCoverage]
    public class WishListRepository : BaseRepository,IWishListRepository
    {
        public WishListRepository(ICassandraConnection cassandraConnection) : base(cassandraConnection) { }

        public async Task<WishList> GetWishList(Guid wishlistId)
        {
            var result = await this.SingleAsync<Models.WishList>(Cql.New("where wishlist_id=?", wishlistId));
            return result==null ? null :new WishList().UpdateWith(result);
        }

        public async Task InsertAsync(WishList wishList)
        {
            var wishListRepo = new Models.WishList().UpdateWith(wishList);

            await this.InsertAsync<Models.WishList>(wishListRepo);
        }

        public async Task UpdateAsync(WishList wishList)
        {
            var wishListRepo = new Models.WishList().UpdateWith(wishList);

            await this.UpdateAsync<Models.WishList>(wishListRepo);
        }

    }
}
