﻿namespace Farfetch.Wishlist.Data.Repository.Models
{
    using System;

    internal abstract class ModelBase
    {
        public Guid Id { get; set; }
    }
}
