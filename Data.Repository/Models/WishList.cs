﻿namespace Farfetch.Wishlist.Data.Repository.Models
{
    using System;
    using System.Collections.Generic;
    internal class WishList : ModelBase
    {
        public WishList()
        {
            Items = new Dictionary<Guid, WishListItem>();
        }
        public int UserId { get; set; }
        public int TenantId {get;set;}
        public DateTime CreatedAt { get; set; }
        public virtual IDictionary<Guid,WishListItem> Items { get; set; }
    }
}
