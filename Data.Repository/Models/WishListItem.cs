﻿namespace Farfetch.Wishlist.Data.Repository.Models
{
    using System;

    internal class WishListItem : ModelBase
    {
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
        public string ProductName { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset UpdatedAt { get; set; }
    }
}
