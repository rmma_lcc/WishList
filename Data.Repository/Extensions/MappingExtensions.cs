﻿namespace Farfetch.Wishlist.Data.Repository.Extensions
{
    using System;
    using System.Collections.Generic;
    using Farfetch.Wishlist.Domain.Model;
    
    internal static class MappingExtensions
    {
        internal static WishListItem UpdateWith(this WishListItem target, Models.WishListItem source)
        {
            target.Id = source.Id;
            target.ProductId = source.ProductId;
            target.ProductName = source.ProductName;
            target.Quantity = source.Quantity;
            target.CreatedAt = source.CreatedAt.DateTime;
            target.UpdatedAt = source.UpdatedAt.DateTime;

            return target;
        }

        internal static WishList UpdateWith(this WishList target, Models.WishList source)
        {
            target.Id = source.Id;
            target.TenantId = source.TenantId;
            target.UserId = source.UserId;
            target.CreatedAt = source.CreatedAt;


            foreach (var item in source.Items)
            {
                target.Items.Add(new WishListItem().UpdateWith(item.Value));
            }

            return target;
        }

        internal static Models.WishListItem UpdateWith(this Models.WishListItem target, WishListItem source)
        {
            target.Id = source.Id;
            target.ProductId = source.ProductId;
            target.ProductName = source.ProductName;
            target.Quantity = source.Quantity;
            target.CreatedAt = source.CreatedAt;
            target.UpdatedAt = source.UpdatedAt;

            return target;
        }

        internal static Models.WishList UpdateWith(this Models.WishList target, WishList source)
        {
            target.Id = source.Id;
            target.TenantId = source.TenantId;
            target.UserId = source.UserId;
            target.CreatedAt = source.CreatedAt;


            foreach (var item in source.Items)
            {
                target.Items[item.Id] = new Models.WishListItem().UpdateWith(item);
            }

            return target;
        }


    }
}
