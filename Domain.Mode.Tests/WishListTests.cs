﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Model.Tests
{
    [TestClass]
    public class WishListTests
    {
        [TestMethod, TestCategory("Domain.Model")]
        public void AddItem(Guid productId, string productName, int quantity)
        {
            var productIdFind = this.Items.Find(item => item.ProductId == productId);
            Guard.Against<AlreadyExistsException>(productIdFind.IsNotNull(), "Product already exists.");

            var wishListItem = new WishListItem(productId, productName, quantity);

            this.Items.Add(wishListItem);
        }
    }
}
