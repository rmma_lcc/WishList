﻿namespace Data.Repository.Tests.Extensions
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Farfetch.Wishlist.Data.Repository.Models;
    using Farfetch.Wishlist.Data.Repository.Extensions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class MappingExtensionsTests
    {
        
        [TestMethod, TestCategory("Data.Repository")]
        public void MappingExtension_WishListItemDomain_UpdateWith_WishListItemRepository()
        {
            WishListItem wishListItemRepository = new WishListItem()
            {
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                ProductId = Guid.NewGuid(),
                ProductName = "novo_produto",
                Quantity = 112
            };

            var wishListItemDomain = new Farfetch.Wishlist.Domain.Model.WishListItem().UpdateWith(wishListItemRepository);

            Assert.AreEqual(wishListItemDomain.Id, wishListItemRepository.Id);
            Assert.AreEqual(wishListItemDomain.ProductId, wishListItemRepository.ProductId);
            Assert.AreEqual(wishListItemDomain.ProductName, wishListItemRepository.ProductName);
            Assert.AreEqual(wishListItemDomain.Quantity, wishListItemRepository.Quantity);
            Assert.AreEqual(wishListItemDomain.UpdatedAt, wishListItemRepository.UpdatedAt);
            Assert.AreEqual(wishListItemDomain.CreatedAt, wishListItemRepository.CreatedAt);
        }

        [TestMethod, TestCategory("Data.Repository")]
        public void MappingExtension_WishListDomain_UpdateWith_WishListRepository()
        {
            WishList wishListRepository = new WishList()
            {
                CreatedAt = DateTime.UtcNow,
                TenantId = 112,
                UserId = 991,
                Items = new Dictionary<Guid, WishListItem>()
            };

            WishListItem wishlistItem = new WishListItem()
            {
                ProductId = new Guid("C21BEF93-026D-41AA-A957-BCA3934355E1"),
                ProductName = "new_product",
                Quantity=112,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow
            };

            wishListRepository.Items.Add(wishlistItem.Id, wishlistItem);

            var wishListDomain = new Farfetch.Wishlist.Domain.Model.WishList().UpdateWith(wishListRepository);

            Assert.AreEqual(wishListRepository.Id, wishListDomain.Id);
            Assert.AreEqual(wishListRepository.CreatedAt, wishListDomain.CreatedAt);
            Assert.AreEqual(wishListRepository.UserId, wishListDomain.UserId);
            Assert.AreEqual(wishListRepository.TenantId, wishListDomain.TenantId);
            Assert.IsTrue(wishListRepository.Items.Values.ToList().Count == wishListDomain.Items.Count);
            Assert.AreEqual(wishListRepository.Items.Values.ToList()[0].Id, wishListDomain.Items[0].Id);
            Assert.AreEqual(wishListRepository.Items.Values.ToList()[0].CreatedAt, wishListDomain.Items[0].CreatedAt);
            Assert.AreEqual(wishListRepository.Items.Values.ToList()[0].ProductId, wishListDomain.Items[0].ProductId);
            Assert.AreEqual(wishListRepository.Items.Values.ToList()[0].ProductName, wishListDomain.Items[0].ProductName);
            Assert.AreEqual(wishListRepository.Items.Values.ToList()[0].Quantity, wishListDomain.Items[0].Quantity);
            Assert.AreEqual(wishListRepository.Items.Values.ToList()[0].UpdatedAt, wishListDomain.Items[0].UpdatedAt);
        }

        [TestMethod, TestCategory("Data.Repository")]
        public void MappingExtension_WishListItemRepository_UpdateWith_WishListItemDomain()
        {
            Farfetch.Wishlist.Domain.Model.WishListItem wishListItemDomain = new Farfetch.Wishlist.Domain.Model.WishListItem()
            {
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                ProductId = Guid.NewGuid(),
                ProductName = "novo_produto",
                Quantity = 112
            };

            var wishListItemRepository = new WishListItem().UpdateWith(wishListItemDomain);

            Assert.AreEqual(wishListItemDomain.Id, wishListItemRepository.Id);
            Assert.AreEqual(wishListItemDomain.ProductId, wishListItemRepository.ProductId);
            Assert.AreEqual(wishListItemDomain.ProductName, wishListItemRepository.ProductName);
            Assert.AreEqual(wishListItemDomain.Quantity, wishListItemRepository.Quantity);
            Assert.AreEqual(wishListItemDomain.UpdatedAt, wishListItemRepository.UpdatedAt);
            Assert.AreEqual(wishListItemDomain.CreatedAt, wishListItemRepository.CreatedAt);
        }

        [TestMethod, TestCategory("Data.Repository")]
        public void MappingExtension_WishListRepository_UpdateWith_WishListDomain()
        {
            Farfetch.Wishlist.Domain.Model.WishList wishListDomain = new Farfetch.Wishlist.Domain.Model.WishList()
            {
                CreatedAt = DateTime.UtcNow,
                TenantId = 112,
                UserId = 991,
                Items = new List<Farfetch.Wishlist.Domain.Model.WishListItem>()
                {
                    new Farfetch.Wishlist.Domain.Model.WishListItem()
                    {
                        ProductId = new Guid("C21BEF93-026D-41AA-A957-BCA3934355E1"),
                        ProductName = "new_product",
                        Quantity = 112,
                        CreatedAt = DateTime.UtcNow,
                        UpdatedAt = DateTime.UtcNow
                    }
                }
            };

            var wishListRepository = new WishList().UpdateWith(wishListDomain);

            Assert.AreEqual(wishListRepository.Id, wishListDomain.Id);
            Assert.AreEqual(wishListRepository.CreatedAt, wishListDomain.CreatedAt);
            Assert.AreEqual(wishListRepository.UserId, wishListDomain.UserId);
            Assert.AreEqual(wishListRepository.TenantId, wishListDomain.TenantId);
            Assert.IsTrue(wishListRepository.Items.Values.ToList().Count == wishListDomain.Items.Count);
            Assert.AreEqual(wishListRepository.Items.Values.ToList()[0].Id, wishListDomain.Items[0].Id);
            Assert.AreEqual(wishListRepository.Items.Values.ToList()[0].CreatedAt, wishListDomain.Items[0].CreatedAt);
            Assert.AreEqual(wishListRepository.Items.Values.ToList()[0].ProductId, wishListDomain.Items[0].ProductId);
            Assert.AreEqual(wishListRepository.Items.Values.ToList()[0].ProductName, wishListDomain.Items[0].ProductName);
            Assert.AreEqual(wishListRepository.Items.Values.ToList()[0].Quantity, wishListDomain.Items[0].Quantity);
            Assert.AreEqual(wishListRepository.Items.Values.ToList()[0].UpdatedAt, wishListDomain.Items[0].UpdatedAt);
        }

    }
}
